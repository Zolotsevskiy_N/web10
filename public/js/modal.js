    $(document).ready(function($) {
      $('.btn-primary').click(function() {
        $('.popup-fade').fadeIn();
        getDataStorage();
        return false;
      });	
      
      $('#popup-close').click(function() {
        history.go(-1);
        $(this).parents('.popup-fade').fadeOut();
        return false;
      });		
     
      $(document).keydown(function(e) {
        if (e.keyCode === 27) {
          e.stopPropagation();
          $('.popup-fade').fadeOut();
        }
      });
      
      $('.popup-fade').click(function(e) {
        if ($(e.target).closest('.popup').length == 0) {
          $(this).fadeOut();					
        }
      });
    });

    $(".btn-primary").click(function(){
    history.pushState(1, "title", "#form_link");
    });

    window.onpopstate = function(event) {
      if(event.state==1) {
        $('.popup-fade').fadeIn();
        getDataStorage();
      }
      else {
        $('.popup-fade').fadeOut();
      }
    };

    function updateStorage() {
      localStorage.setItem("name", document.getElementById("name").value);
      localStorage.setItem("phone", document.getElementById("phone").value);
      localStorage.setItem("region", document.getElementById("region").value);
      localStorage.setItem("comment", $("#comment").val());
    }

    function getDataStorage() {
      document.getElementById("name").value = localStorage.getItem("name");
      document.getElementById("phone").value = localStorage.getItem("phone");
      if(localStorage.getItem("region")==null) {
        document.getElementById("reg").selected = true;
      }
      else {
        document.getElementById("region").value = localStorage.getItem("region");
      }
      $("#comment").val(localStorage.getItem("comment"));
    }

    window.addEventListener("DOMContentLoaded", function (event) {
      let profit = document.getElementById("form_link");
      profit.addEventListener("input", function(event) {
        updateStorage();
      });
    });   
    
    $(function(){
        $(".ajaxForm").submit(function(e){
            e.preventDefault();
            var href = $(this).attr("action");
            $.ajax({
                type: "POST",
                dataType: "json",
                url: href,
                data: $(this).serialize(),
                success: function(response){
                    if(response.status == "success"){
                        alert("Вы успешно отправили свои данные!");
                        localStorage.clear();
                        document.getElementById("name").value = "";
                        document.getElementById("phone").value = "";
                        document.getElementById("reg").selected = true;
                        $("#comment").val(localStorage.getItem(""));
                    }else{
                        alert("Ошибка отправки: " + response.message);
                    }
                }
            });
        });
    });
    
    